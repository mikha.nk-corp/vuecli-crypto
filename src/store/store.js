import { createStore } from 'vuex'
import {getRequestNews} from './requests/requestNews.js'
import coinsStore from './modules/coinsStore.js'

const store = createStore({
    modules: {coinsStore},
    state: {
        isWeatherOpen: null,
        chosenCity: 'Odesa',
        isSideMenuOpen: null,
        newsList: [],
    }, 
    getters: {

        isWeatherOpen(state) {
            return state.isWeatherOpen
        },
        chosenCity(state) {
            return state.chosenCity
        },
        isSideMenuOpen(state) {
            return state.isSideMenuOpen
        },
        newsList(state) {
            return state.newsList
        }
    },
    mutations: {
        weatherOpenChange(state) {
            state.isWeatherOpen ? state.isWeatherOpen = false : state.isWeatherOpen = true
        },
        sideMenuOpenChange(state) {
            state.isSideMenuOpen ? state.isSideMenuOpen = false : state.isSideMenuOpen = true
        },
        updateNewsList (state, newsList) {
            state.newsList = newsList
        },
        changeChosenCity(state, city) {
            state.chosenCity = city
        },
    },
    actions: {
        async getCryptoNews (ctxt) {
           const newsList = await getRequestNews()
           ctxt.commit('updateNewsList', newsList)
        },
    }
})
export default store

