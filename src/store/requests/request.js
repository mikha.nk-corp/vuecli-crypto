import axios from 'axios';
export async function getCryptoCurrency(coin) {
    const res = await axios.get(`https://min-api.cryptocompare.com/data/price?fsym=${coin}&tsyms=USD&api_key=e86ad2b821b4fe0801179ebfa7e33be649f35bdfc77c6695eaa29c37a029cc71`)
    const prise = res.data
    return prise
}