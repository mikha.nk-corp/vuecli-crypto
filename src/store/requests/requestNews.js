import axios from 'axios';
export async function getRequestNews() {
    const res = await axios.get(`https://min-api.cryptocompare.com/data/v2/news/?lang=EN&api_key=e86ad2b821b4fe0801179ebfa7e33be649f35bdfc77c6695eaa29c37a029cc71`)
    const newsData = res.data.Data
    let newsTitles = []
    for (let i=0; i < 10; i++) {
       const title = {title: newsData[i].title} 
       newsTitles.push(title)
    }
    return newsTitles
}