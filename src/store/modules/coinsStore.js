import {getCryptoCurrency} from '../requests/request';

export default {
    state: {
        coins: [
            { name: 'Bitcoin', symbol: 'BTC', currentPrice: null, lastPrice: 1, pricesArr: [], difference: '', isFav: false, isSelected: null, logourl: 'https://resources.cryptocompare.com/asset-management/1/1659708726266.png'},
            { name: 'Ethereum', symbol: 'ETH', currentPrice: null, lastPrice: 1, pricesArr: [], difference: '', isFav: false, isSelected: null, logourl: 'https://resources.cryptocompare.com/asset-management/2/1659946678476.png'},
            { name: 'Tether', symbol: 'USDT', currentPrice: null, lastPrice: 1, pricesArr: [], difference: '', isFav: false, isSelected: null, logourl: 'https://resources.cryptocompare.com/asset-management/7/1661244968930.png'},
            { name: 'Dogecoin', symbol: 'DOGE', currentPrice: null, lastPrice: 1, pricesArr: [], difference: '', isFav: false, isSelected: null, logourl: 'https://resources.cryptocompare.com/asset-management/26/1662541306654.png'},
        ],
        isCoinsDisabled: false,
        favList: []
    }, 
    getters: {
        coinsCards(state) {
            return state.coins
        },
        isCoinsDisabled(state) {
            return state.isCoinsDisabled
        },
        favList(state) {
            return state.favList
        },
    },
    mutations: {
        updateCoins (state, coins) {
            state.coins = coins
        },
        addingCoin (state, coin) {
            state.coins.push(coin)
        },
        isSelectedChange(state, coin) {
            if (coin.isSelected) {
                coin.isSelected = false
            }
            else {
                for (let item of state.coins) {
                    item.isSelected = false
                }
                coin.isSelected = true
            }
        },
        CoinsDisabledChange(state, value) {
            state.isCoinsDisabled = value;
        },
        showFav (state) {
            state.coins = state.favList
        },
        clearFav (state) {
            state.favList = []
            state.coins.forEach(coin => coin.isFav = false) 
            localStorage.clear()
        },
        getFavList(state) {
            const localList = localStorage.getItem ('favList')
            const favListLS = JSON.parse(localList)
            state.favList = favListLS
            if (state.favList) {
                state.favList.forEach(favItem => {
                    state.coins.forEach(coin => {
                        if (favItem.symbol === coin.symbol) {
                            coin.isFav = true;
                        }
                    });
                });
            }
        },
        addToFav (state, coin) {
            if (!state.favList) {
                state.favList = [];
            }
            console.log('заходит')
            console.log(typeof state.favList)
            coin.isFav = !coin.isFav
            if (coin.isFav) {
                state.favList.push(coin)
            }
            else {
                const index = state.favList.findIndex(favcoin => favcoin.symbol === coin.symbol);
                if (index !== -1) {
                    state.favList.splice(index, 1);
                }
            }
            console.log(state.favList)
            localStorage.setItem('favList', JSON.stringify(state.favList))
        },
    },
    actions: {
        async getPrices(ctxt, coins) {
            if (coins) {
                for (let coin of coins) {
                const price = await getCryptoCurrency(coin.symbol);
                coin.currentPrice = price.USD
                if (coin.currentPrice > coin.lastPrice) {coin.difference = 'up'}
                else if (coin.currentPrice < coin.lastPrice) {coin.difference = 'down'}
                else {{coin.difference = ''}}
                if (coin.currentPrice != coin.lastPrice) {
                    coin.pricesArr.push(price.USD)
                }
                if (coin.pricesArr > 30) {coin.pricesArr.shift()}
                coin.lastPrice = coin.currentPrice
                }
                ctxt.commit('updateCoins', coins)
            }
        },
    }
}