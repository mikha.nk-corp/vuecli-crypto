import axios from 'axios';
const key = 'b0e46230244c77c0a2971a8cdbdc5bd5'
export async function getCityWeather(city) {
    const res = await axios.get(`https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${key}&units=metric`)
    const data = res.data
    return data
}