import generalList from './coinsList.json'
/* import {getLogo} from './getLogo.js' */

export async function getCoinsShortList() {
    
    const coinsShortList = []
    for (let i = 0; i < generalList.length; i++) {
/*         const logourl = await getLogo(generalList[i].symbol) */
        const coinInfo = {name: generalList[i].name, symbol: generalList[i].symbol, /* logourl: logourl */} 
        coinsShortList.push(coinInfo) 
    } 
    return coinsShortList
}