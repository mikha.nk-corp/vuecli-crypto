import axios from 'axios';
export async function getLogo(coin) {
    const res = await axios.get(`https://data-api.cryptocompare.com/asset/v1/data/by/symbol?asset_symbol=${coin}&api_key=e86ad2b821b4fe0801179ebfa7e33be649f35bdfc77c6695eaa29c37a029cc71`)
    const logourl = res.data.Data.LOGO_URL
    return logourl
}