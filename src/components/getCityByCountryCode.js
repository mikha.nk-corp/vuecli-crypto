import generalList from './cityList.json'
export function getCity() {
    let general = generalList
    let citiesShortList = []
    let citiesList =  [...general].filter(city => city.country.toLowerCase().includes('ro'))
    for (let city of citiesList) {
        let shortInfo = {
            name: city.name,
            id: city.id,
            country: city.country
        }
        citiesShortList.push(shortInfo) 
    }

    return citiesShortList
} 