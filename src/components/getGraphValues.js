export function getGraphValues (tempArr) {
    console.log('то что приходит в функцию')
    console.log(tempArr)
    if (tempArr.length > 1) {
    const returnArr = []
    let maxPrice = Math.max(...tempArr)
    let minPrice = Math.min(...tempArr)
    let tempMargin = 0
    for (let i = 1; i < tempArr.length; i++) {
        const change = (tempArr[i] - tempArr[i-1])/(maxPrice - minPrice)*100
        const day = {
            price: tempArr[i],
            difference: tempArr[i] - tempArr[i-1],
            margin: tempMargin,
            change: change,
            absChange: Math.abs(change)
        }
        returnArr.push(day)
        tempMargin += change
    }
    return returnArr
    }
    else {return undefined} 
}