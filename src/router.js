import { createRouter, createWebHistory } from "vue-router";
import CryptoNews from './components/news/CryptoNews.vue'
import CryptoConverter from './components/Converter/CryptoConverter.vue'
import store from './store/store';

export default createRouter ({
    history: createWebHistory(),
    routes: [
        { 
            path: '/',
            beforeEnter: (to, from, next) => {
                store.commit('CoinsDisabledChange', false);
                next();
            }
        },
        { 
            path: '/news',
            component: CryptoNews,
            beforeEnter: (to, from, next) => {
                store.commit('CoinsDisabledChange', true);
                next();
            }
        },
        { 
            path: '/converter', 
            component: CryptoConverter,
            beforeEnter: (to, from, next) => {
                console.log('router function')
                store.commit('CoinsDisabledChange', true);
                next();
            }
        },
    ]
})
