module.exports = {
  devServer: {
    proxy: {
      '/api': {
        target: 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest',
        changeOrigin: true,
        pathRewrite: { '^/api': '' },
        headers: {
          'X-CMC_PRO_API_KEY': 'a832f38a-82e2-4402-9a32-d7daa294875d',
        },
      },
    },
  },
};