const express = require('express');
const axios = require('axios');

const app = express();

const API_KEY = 'a832f38a-82e2-4402-9a32-d7daa294875d'; 

app.use(express.json());

app.get('/api/cryptocurrency/listings/latest', async (req, res) => {
  try {
    const response = await axios.get('https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest', {
      headers: {
        'X-CMC_PRO_API_KEY': API_KEY
      }
    });
    res.json(response.data);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

const PORT = 3000;
app.listen(PORT, () => {
  console.log(`Proxy server is listening on port ${PORT}`);
});